//
//  ViewController.swift
//  Todo
//
//  Created by Petrus on 6/12/19.
//  Copyright © 2019 Petrus. All rights reserved.
//

import UIKit
import  RealmSwift


class MainVC: UIViewController {
    
    @IBOutlet weak var todoTableView: UITableView!

    let items: [Results<Todo>] = [Todo.allTodos(completed: false), Todo.allTodos(completed: true)]
    let titles = ["Uncompleted", "Completed"]
    private var itemsTokens = [NotificationToken]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        todoTableView.dataSource = self
        todoTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTokens = items.map { [weak self] item in
            item.observe { changes in
                guard let tableView = self?.todoTableView else { return }
                
                switch changes {
                case .initial:
                    tableView.reloadData()
                    break
                case .update:
                    tableView.reloadSections(IndexSet([0, 1]), with: .automatic)
                    break
                case .error: break
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        itemsTokens.forEach { $0.invalidate() }
    }
}


// MARK: - TableView Data Source
extension MainVC: UITableViewDataSource {
    private func toggleItem(_ item: Todo) {
        item.update(title: item.title, completed: !item.completed)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "todo_cell", for: indexPath) as! TodoTableViewCell
        let todo = items[indexPath.section][indexPath.row]
        cell.bind(todo) { [weak self] item in
            self?.toggleItem(item)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].isEmpty ? nil : titles[section]
    }
}

// MARK: - TableView Delegate
extension MainVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let alertController = UIAlertController(title: "Delete item", message: "Are you sure you want to delete this todo?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.items[indexPath.section][indexPath.row].delete()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addOrUpdateVC = self.storyboard!.instantiateViewController(withIdentifier: "AddOrUpdateVC") as! AddOrUpdateVC
        addOrUpdateVC.todo = items[indexPath.section][indexPath.row]
        self.navigationController?.pushViewController(addOrUpdateVC, animated: true)
    }
    
}


extension UITableView {
    func applyChanges(deletions: [Int], insertions: [Int], updates: [Int], section: Int) {
        performBatchUpdates({
            deleteRows(at: deletions.map { IndexPath(row: $0, section: section) }, with: .automatic)
            insertRows(at: insertions.map { IndexPath(row: $0, section: section) }, with: .automatic)
            reloadRows(at: updates.map { IndexPath(row: $0, section: section) }, with: .automatic)
        })
    }
}
